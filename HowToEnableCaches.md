# Hibernate and Second Level Caches

This document is going to assume that we are going to be working with Hibernate `6.3.1.Final` and you have just the below maven dependency.

So let's assume that we have the below property defined in our pom file.

```xml
<properties>
	<hibernate.version>6.3.1.Final</hibernate.version>
</properties>
```


```xml
<dependency>
    <groupId>org.hibernate.orm</groupId>
    <artifactId>hibernate-core</artifactId>
    <version>${hibernate.version}</version>
</dependency>
```

## Common Maven Dependencies

We need to be adding the below Maven dependency.

```xml
<dependency>
    <groupId>org.hibernate</groupId>
    <artifactId>hibernate-jcache</artifactId>
    <version>${hibernate.version}</version>
</dependency>
```

This dependency adds up an implementation of `org.hibernate.boot.registry.selector.StrategyRegistrationProvider`, which internally forces hibernate to start looking for implementation of `javax.cache.spi.CachingProvider` and thus ends up finding a proper implementation from the **CLASSPATH**.


## Working with Infinispan


Now we will add **Infinispan** implementation of `javax.cache.spi.CachingProvider` via the below maven dependency.

```xml
<dependency>
    <groupId>org.infinispan</groupId>
    <artifactId>infinispan-hibernate-cache-v62</artifactId>
    <version>14.0.17.Final</version>
    <exclusions>
        <exclusion>
            <artifactId>hibernate-core</artifactId>
            <groupId>org.hibernate.orm</groupId>
        </exclusion>
        <exclusion>
            <artifactId>jakarta.persistence-api</artifactId>
            <groupId>jakarta.persistence</groupId>
        </exclusion>
    </exclusions>
</dependency>
<dependency>
    <groupId>org.infinispan</groupId>
    <artifactId>infinispan-jcache</artifactId>
    <version>14.0.17.Final</version>
</dependency>
```

The following properties can be enabled via the `hibernate-cfg.xml` (or) via the implementation of `jakarta.persistence.spi.PersistenceUnitInfo`.

```java
Properties properties = new Properties();
//This generates a lot of statistics on the console which can be reviewed.
properties.setProperty("hibernate.generate_statistics", "true");
// Use Infinispan second level cache provider
// Refer https://infinispan.org/docs/stable/titles/hibernate/hibernate.html#single_node_standalone_hibernate_application
properties.setProperty("hibernate.cache.region.factory_class", "infinispan");
//Enabling second-level caches
properties.setProperty("hibernate.cache.use_second_level_cache", "true");

//Indicates what should hibernate do, if in case it finds a cache to be missing.
//Read more at https://docs.jboss.org/hibernate/orm/6.1/userguide/html_single/Hibernate_User_Guide.html#caching-provider-jcache-missing-cache-strategy
properties.setProperty("hibernate.javax.cache.missing_cache_strategy", "create-warn");
```
To run the sample test using this cache implementation run the below maven command:

```bash
mvn -Pinfinispan -Dtest=JPACachingDemoTest clean test
```

## Working with Ehcache v3

Add **Ehcache** implementation of `javax.cache.spi.CachingProvider` via the below maven dependency.

```xml
<dependency>
    <groupId>org.ehcache</groupId>
    <artifactId>ehcache</artifactId>
    <version>3.9.6</version>
</dependency>
```

The following properties can be enabled via the `hibernate-cfg.xml` (or) via the implementation of `jakarta.persistence.spi.PersistenceUnitInfo`.

```java
Properties properties = new Properties();
//This generates a lot of statistics on the console which can be reviewed.
properties.setProperty("hibernate.generate_statistics", "true");
// Use Infinispan second level cache provider
// Refer https://infinispan.org/docs/stable/titles/hibernate/hibernate.html#single_node_standalone_hibernate_application
properties.setProperty("hibernate.cache.region.factory_class", "jcache");
//Enabling second-level caches
properties.setProperty("hibernate.cache.use_second_level_cache", "true");

//Indicates what should hibernate do, if in case it finds a cache to be missing.
//Read more at https://docs.jboss.org/hibernate/orm/6.1/userguide/html_single/Hibernate_User_Guide.html#caching-provider-jcache-missing-cache-strategy
properties.setProperty("hibernate.javax.cache.missing_cache_strategy", "create-warn");
```
To run the sample test using this cache implementation run the below maven command:

```bash
mvn -Peh-cache -Dtest=JPACachingDemoTest clean test
```

## Working with Caffeine

Add **Caffeine** implementation of `javax.cache.spi.CachingProvider` via the below maven dependency.

```xml
<dependency>
    <groupId>com.github.ben-manes.caffeine</groupId>
    <artifactId>jcache</artifactId>
    <version>3.1.8</version>
</dependency>
```

The following properties can be enabled via the `hibernate-cfg.xml` (or) via the implementation of `jakarta.persistence.spi.PersistenceUnitInfo`.

```java
Properties properties = new Properties();
//This generates a lot of statistics on the console which can be reviewed.
properties.setProperty("hibernate.generate_statistics", "true");
// Use Infinispan second level cache provider
// Refer https://infinispan.org/docs/stable/titles/hibernate/hibernate.html#single_node_standalone_hibernate_application
properties.setProperty("hibernate.cache.region.factory_class", "jcache");
//Enabling second-level caches
properties.setProperty("hibernate.cache.use_second_level_cache", "true");

//Indicates what should hibernate do, if in case it finds a cache to be missing.
//Read more at https://docs.jboss.org/hibernate/orm/6.1/userguide/html_single/Hibernate_User_Guide.html#caching-provider-jcache-missing-cache-strategy
properties.setProperty("hibernate.javax.cache.missing_cache_strategy", "create-warn");
```

To run the sample test using this cache implementation run the below maven command:

```bash
mvn -Pcaffeine -Dtest=JPACachingDemoTest clean test
```

## Working with Hazelcast

Add ** Hazelcast** implementation of `javax.cache.spi.CachingProvider` via the below maven dependency.

```xml
<dependency>
    <groupId>com.hazelcast</groupId>
    <artifactId>hazelcast</artifactId>
    <version>5.3.2</version>
</dependency>
```

The following properties can be enabled via the `hibernate-cfg.xml` (or) via the implementation of `jakarta.persistence.spi.PersistenceUnitInfo`.

```java
Properties properties = new Properties();
//This generates a lot of statistics on the console which can be reviewed.
properties.setProperty("hibernate.generate_statistics", "true");
// Use Infinispan second level cache provider
// Refer https://infinispan.org/docs/stable/titles/hibernate/hibernate.html#single_node_standalone_hibernate_application
properties.setProperty("hibernate.cache.region.factory_class", "jcache");
//Enabling second-level caches
properties.setProperty("hibernate.cache.use_second_level_cache", "true");

//Indicates what should hibernate do, if in case it finds a cache to be missing.
//Read more at https://docs.jboss.org/hibernate/orm/6.1/userguide/html_single/Hibernate_User_Guide.html#caching-provider-jcache-missing-cache-strategy
properties.setProperty("hibernate.javax.cache.missing_cache_strategy", "create-warn");
```

To run the sample test using this cache implementation run the below maven command:

```bash
mvn -Phazelcast -Dtest=JPACachingDemoTest clean test
```
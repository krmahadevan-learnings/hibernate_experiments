## Youtube playlists 

- [JPA/Hibernate Fundamentals 2023](https://www.youtube.com/playlist?list=PLEocw3gLFc8UYNv0uRG399GSggi8icTL6)
- [JPA/Fundamentals 2019](https://www.youtube.com/playlist?list=PLEocw3gLFc8USLd90a_TicWGiMThDtpOJ)


## Points to remember

#### `Owner of the relation` - The entity/table where the foreign key will be created is called as the owner of the relation. 

#### `OneToMany` (Unidirectional) with Join tables.

Suppose we have 

```java
@Entity
public class Vehicle {
   // other code
   
    @OneToMany(cascade = {CascadeType.PERSIST})
    private Set<VehiclePart> parts;
    
   // rest of the class 
}
```

And the below relationships are established.

- `Vehicle ===> VehiclePart` - PRESENT
- `VehiclePart --> Vehicle`  - ABSENT

Here we are saying that:

- One vehicle has many parts.
- This is a Unidirectional relationship, because `VehiclePart` in this case is NOT having any reference to `Vehicle`
- Since `VehiclePart ---> Vehicle` reference is missing, JPA will end up creating an intermediatory `Join Table` which will contain the Primary Keys of both `Vehicle` and `VehiclePart` 


#### `OneToMany` (Unidirectional) without Join tables.

Suppose we have 

```java
@Entity
public class Vehicle {
   // other code
   
    @OneToMany(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "vehicle_part_id") // name is just for our purpose
    private Set<VehiclePart> parts;
    
   // rest of the class 
}
```

And the below relationships are established.

- `Vehicle ===> VehiclePart` - PRESENT
- `VehiclePart --> Vehicle`  - PRESENT

Here we are saying that:

- One vehicle has many parts.
- This is a Unidirectional relationship (From Java Objects perspective ONLY), because `VehiclePart` in this case is NOT having any reference to `Vehicle`
- `VehiclePart ---> Vehicle` is established via the `@JoinColumn` on `parts` data member.
- On seeing `@JoinColumn` on a `@OneToMany` annotated field, JPA will create a foreign key in the `VehiclePart` entity's table with the column name as `vehicle_part_id` and store the `Vehicle` entity's primary key value for every insertion that happens into `VehiclePart` table.

#### `OneToMany` (Bidirectional) without Join tables.

Suppose we have 

```java
@Entity
public class Vehicle {
   // other code
   
    @OneToMany(mappedBy = "vehicle", cascade = {CascadeType.PERSIST})
    private Set<VehiclePart> parts;
    
   // rest of the class 
}

@Entity
public class VehiclePart {
   // other code

    @ManyToOne
    private Vehicle vehicle;
    
   // rest of the class 
}
```

And the below relationships are established.

- `Vehicle ===> VehiclePart` - PRESENT
- `VehiclePart --> Vehicle`  - PRESENT

Here we are saying that:

- One vehicle has many parts.
- One part belongs to ONLY one vehicle.
- This is a Bidirectional relationship.
- `VehiclePart ---> Vehicle` is established via the `@ManyToOne` on `vehicle` data member in `VehiclePart`

## Important Annotations

- `@Entity` - Marks a POJO as a representation of a table in Database
- `@Basic` - Optional annotation. Lets you do a basic mapping of a pojo field to a column in db. (basically you can say if a field is to be loaded eagerly or lazily and if a field is optional)
- `@Id` - Marks a field as a primary key
- `@GeneratedValue` - Marks a field as auto generated backed by some auto generation strategy as supported by the underlying database.
- `@Table` - allows you to specify explicitly the table name, if the POJO and the table name differ from each other.
- `@Column` - Lets you define POJO field to table column mapping (nullable, length, table to which column belongs to, explicit column name mapping when field name and colum names are different)
- `@Enumerated` - Allows a field to be persisted as an enum value (Either as its text or its cardinal value
- `@Temporal` - Use this annotation to earmark a entity data member to be a date/time/timestamp
- `@MapKeyEnumerated` - This annotation is usually used along with `@OneToMany` relation building annotations, which translates to a map. If the key of this map is an enum and if its to be treated as an enum at the DB column level then this annotation comes into picture.
- `@Embeddable` - When a class is annotated with this annotation, then it means that it's data members represent columns of some table.
- `@Embedded` - When a field is annotated with this annotation, it means that its attributes represent some columns that are going to be part of the current table. The class that is used as the type of the field should be annotated using `@Embeddable`
- `@EmbeddedId` - When a field is annotation with this annotation, then the type of the field should be an `@Embeddable` type, and thus this field becomes a composite key for the entity.
- `@IdClass(Name.class)` - When a class is annotated using this annotation, then we are basically saying that all the data members of its value (here `Name.class`) come together to represent a composite primary key. Here :
    - `Name.class` must implement the `Serializable` interface 
    - The data members of `Name.class` must match the current entity's data members.
    - All the current entity's data members which are also part of `Name.class` must be annotated with `@Id` annotation to tell the JPA implementation that there are multiple attributes to be considered when forming the primary key.
- `@AttributeOverride` - Use this annotation when you would like to override an attribute that was defined either in the base class (in the case of the attribute being exposed via `@MappedSuperClass` annotation) or in the embedded class.
- `@AssociationOverride` - This annotation is usually used on a data member that represents an `@Embedded` pojo. When used, it allows us to override the relation attributes that are defined inside the `@Embeddable` POJO. So things such as either `joinColumn` (or) `joinTable` (in the case of a `@ManyToMany` relationship) can be overridden by using this annotation.
- `@MappedSuperclass` - use this annotation on a class that represents the common columns from a bunch of tables via a base class. So if a base class is extended by a bunch of `@Entity` annotated classes, then this annotation should be used on the base class.
- `@Inheritance(strategy = InheritanceType.SINGLE_TABLE)` - When dealing with entity inheritance, use this annotation on the top of the base class. The strategy determines the number of tables that are going to be created.
- `@DiscriminatorColumn(name = "animal_type")` - Use this annotation if you would like to rename the discriminator column (basically the column which captures the type of the entity, that the current row represents) from the default `DTYPE` to something else (here we are naming the discriminator column to `animal_type`)
- `@ElementCollection` - Use this annotation if you would like to map one or more column values to a collection in the current entity. The collection can be a regular data type or it can be a collection of `@Embeddable`. When this annotation is used, it would basically read the data from the table+column combo specified via the `@CollectionTable` and translates a table that contains multiple records for the current entity, into a collection that can be iterated. This is usually used when we don't want to be doing a `@OneToMany` relationship and bringing in the complexities of joins. So in a way this is join without actually using it. Internally the JPA implementation may trigger multiple select statements for the parent entity and the child table which is referenced via the `@JoinTable` annotation.
- `@OneToOne` - Use this annotation to establish a `1:1` relationship with another entity.
- `@JoinColumn` - This annotation is to be used only when a field already has the `@OneToOne` (or) `@OneToMany` relationship annotations. Using this annotation, we can customise the column name and also tweak the name of the constraint, using the `@ForeignKey` attribute of this annotation.
- `@OneToOne(mappedBy = "passport")` - The `mappedBy` attribute will come into picture when we would like to define a bi-directional relationship. This attribute is to be used on the entity that does not own the relationship. So for e.g., if we are defining `Citizen to passport` relationship, then the `mappedBy` attribute will be set on the `OneToOne` annotation inside the `Passport` entity class, and its value will be the field name of the Passport entity attribute present in the `Citizen` class.
- `@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})` - The cascade attribute basically tells the JPA provider as to what should be done to the relationship object when some JPA operation is done on the parent entity. Here we are saying that whenever the parent entity is either persisted or removed from the context, then do the same automatically to the child entity as well.
- `@NamedQuery` - Use this annotation to created JPQL queries accompanied by names, so that you can use `EntityManager.createNamedQuery()` to refer to them. Its something like a mechanism using which one can avoid repeting queries everywhere.
- `@NamedEntityGraph` - This annotation is usually used when you would like to selectively be able to fetch child entities eagerly (when entities deal with `@OneToMany` annotated relationships), on demand. Using this annotation, you would basically list out all the `OneToMany` annotated attributes which ONLY should be fetched eagerly (via joins)
- `@Cacheable` - When an entity is annotated using this annotation and when second level caching has been configured for the JPA implementation, then an entity can be cached easily into the Cache that has been configured (Options include caffeine, infispan, hazelcast etc.,)
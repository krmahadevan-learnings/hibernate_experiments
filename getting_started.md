# My Experiments with Hibernate

This repository basically captures whatever examples I wrote to understand some basic constructs within Hibernate.

Since these are easy to forget, I am collating them into a single repository for future reference.

## Pre-requisites

* JDK17
* Maven
* Docker

## Using the samples

All the samples are basically TestNG driven sample tests that run against a PostGreSQL Database running out of a docker container.

To get the DB up and running, please run the below command from the root directory (the directory where you have the `pom.xml` file).

```bash
docker-compose -f src/main/resources/docker/docker-compose.yml up -d
```

This should bring up a PostGreSQL DB, with a database named `inventory` created.

We make use of maven profiles, so that we can run the same Cache based test against different caching implementations.

Here are the profiles that are available to use:

```bash
mvn help:all-profiles

[INFO] Listing Profiles for Project: com.rationaleemotions:hibernate_experiments:jar:1.0-SNAPSHOT
  Profile Id: infinispan (Active: true , Source: pom)
  Profile Id: eh-cache (Active: false , Source: pom)
  Profile Id: caffeine (Active: false , Source: pom)
  Profile Id: hazelcast (Active: false , Source: pom)
```

For more information about these profiles and their relevance, please read through [How to enable caches](./HowToEnableCaches.md)
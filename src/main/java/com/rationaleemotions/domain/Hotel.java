package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Embedded //Because the POJO we are consuming is an embeddable type.
    @AssociationOverride(
            name = "dishes", // This represents the field name that holds the ManyToMany relationship in the embeddable
            joinTable = @JoinTable(name = "hotels_and_dishes",
                    joinColumns = @JoinColumn(name = "hotel"), // The Join table in the DB is created manually with these column names
                    inverseJoinColumns = @JoinColumn(name = "dish")
            )
    )
    private DishDetails dishDetails;
}

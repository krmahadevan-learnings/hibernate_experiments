package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
@Table(name = "bus_depot")
public class BusDepot {

    @Id
    @GeneratedValue
    private Long id;

    private String location;

    private String supervisor;

    @ElementCollection //Indicates that we would like to access this data member as a collection (List|Set|Map)
    @CollectionTable(
            name = "bus", //"that" table which contains data that is to be transformed into a collection.
            joinColumns = {
                    @JoinColumn(name = "belongs_to") // "that" table's foreign key
            }
    )
    //We are doing the below mappings because there is a NAME MISMATCH between
    // the column in the DB and the data member in the embeddable pojo that we are using here.

    //Mapping pojo.regNumber to registration_number
    @AttributeOverride(name = "regNumber", column = @Column(name = "registration_number"))
    //Mapping pojo.multiAxle to multi_axle
    @AttributeOverride(name = "multiAxle", column = @Column(name = "multi_axle"))
    private List<BusInformation> buses;
}

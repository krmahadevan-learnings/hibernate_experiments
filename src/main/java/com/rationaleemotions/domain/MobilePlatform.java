package com.rationaleemotions.domain;

public enum MobilePlatform {
    IOS,
    ANDROID
}

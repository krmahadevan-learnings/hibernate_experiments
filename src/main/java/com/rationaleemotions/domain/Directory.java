package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@Entity
@NamedQuery(name = Directory.DIRECTORY_FIRST10, query = "Select d from Directory d where d.id <= 10")
@NamedQuery(name = Directory.DIRECTORY_LAST10, query = "Select d from Directory d where d.id between 90 and 100")
public class Directory {

    public static final String DIRECTORY_FIRST10 = "Directory.first10";
    public static final String DIRECTORY_LAST10 = "Directory.last10";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

}

package com.rationaleemotions.domain;

public enum BookType {
    PAPERBACK,
    DIGITAL_PDF,
    AUDIOBOOK
}

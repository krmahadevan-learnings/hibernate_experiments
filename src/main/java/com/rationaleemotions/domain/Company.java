package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @AttributeOverride(name = "doorNumber", column = @Column(name = "house_number"))
    @AttributeOverride(name = "streetName", column = @Column(name = "street"))
    @Embedded
    private Address address;

}

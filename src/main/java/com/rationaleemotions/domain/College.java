package com.rationaleemotions.domain;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Set;

@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@Entity
@NamedEntityGraph(name = "college->courses", attributeNodes = @NamedAttributeNode("courses"))
public class College {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Course> courses;

    @Enumerated(EnumType.STRING)
    private CollegeType type;

    public enum CollegeType {
        ENGINEERING,
        ARTS_AND_SCIENCE,
        MEDICAL
    }
}

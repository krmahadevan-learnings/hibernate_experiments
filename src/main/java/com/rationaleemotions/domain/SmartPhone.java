package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@Entity
@Table(name = "smart_phone")
public class SmartPhone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String manufacturer;

    private String make;
    private String model;
    private String imei;

    @Column(name = "platform") // The column name in the table is different from this field name.
    @Enumerated(EnumType.STRING)
    private MobilePlatform mobilePlatform;

}

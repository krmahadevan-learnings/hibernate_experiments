package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@ToString
@Accessors(chain = true, fluent = true)
@MappedSuperclass // We are indicating that we represent the base class for a bunch of entities.
//The base class columns get included in the tables of the child entities.
@SuppressWarnings("unchecked")
public abstract class Player<T extends Player<?>> {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dob")
    private LocalDateTime dateOfBirth;

    public T id(Long id) {
        this.id = id;
        return (T) this;
    }

    public T name(String name) {
        this.name = name;
        return (T) this;
    }

    public T dateOfBirth(LocalDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return (T) this;
    }
}

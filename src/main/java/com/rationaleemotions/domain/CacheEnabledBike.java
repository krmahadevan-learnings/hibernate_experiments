package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Objects;

@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
@Cacheable
public class CacheEnabledBike extends CommonBikeAttribs<CacheEnabledBike>{


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CacheEnabledBike that = (CacheEnabledBike) o;
        return Objects.equals(id(), that.id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id());
    }
}

package com.rationaleemotions.domain;

import com.fasterxml.uuid.Generators;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.util.UUID;

@Entity
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@ToString
public class Employee {

    @Id
    //This is hibernate specific way of creating a custom generator
    @GenericGenerator(name = "CompanyId", type = CompanyIdGenerator.class)
    //When working with a custom generator, we need to ensure that we refer to the name of the generator
    @GeneratedValue(generator = "CompanyId")
    private UUID id;

    private String name;

    private String address;

    public static class CompanyIdGenerator implements IdentifierGenerator {

        @Override
        public Object generate(SharedSessionContractImplementor session, Object object) {
            return Generators.timeBasedGenerator().generate();
        }
    }

}

package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Getter
@Accessors(fluent = true)
@ToString
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// Creates one common table for all sub-type records named "electronicdevice"
@DiscriminatorColumn(
        name = "device_type" // This will be the column that will hold the type of the device. This is how
        // the JPA differentiates between rows of different sub-classes.
        // The value in this column will basically be the class name of the actual sub-type of the "ElectronicDevice"
        // class.
)
@SuppressWarnings({"unchecked"})
public abstract class ElectronicDevice<T extends ElectronicDevice<?>> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String serialNumber;
    private String name;

    public T Id(Long id) {
        this.id = id;
        return (T) this;
    }

    public T serialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
        return (T) this;
    }

    public T name(String name) {
        this.name = name;
        return (T) this;
    }
}

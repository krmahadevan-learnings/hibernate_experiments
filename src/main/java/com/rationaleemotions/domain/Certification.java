package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@ToString(callSuper = true)
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
public class Certification extends AuditableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "course")
    private String courseName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "certified_date")
    private LocalDateTime certificationDate;

    @Column(name = "issued_by")
    private String certificationIssuer;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "valid_until")
    private LocalDateTime validUntil;

}

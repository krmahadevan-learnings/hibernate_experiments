package com.rationaleemotions.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@IdClass(Name.class) //Indicates that we are going to be using this class as our composite primary key.
// The class should also implement Serializable interface
// When using this approach, we need to ensure that the fields that are part of the "Name.class" should also
// be part of the current entity. So in a way it's like code duplication.
@Getter
@Setter
@ToString
@Accessors(chain = true, fluent = true)
public class Student {

    @Id
    @Column(name = "first_name")
    private String firstName;
    @Id
    @Column(name = "last_name")
    private String lastName;

    private int rollNumber;


}

package com.rationaleemotions.domain;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString(callSuper = true)
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@Entity
public class Mammal extends Animal<Mammal> {

    private boolean isDangerous;
    private boolean isDomesticable;
}

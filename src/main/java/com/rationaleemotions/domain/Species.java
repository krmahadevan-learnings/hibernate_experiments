package com.rationaleemotions.domain;

public enum Species {
    AMPHIBIAN,
    MAMMAL,
    REPTILE,
    BIRD,
    AQUATIC
}

package com.rationaleemotions.domain;

import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString(callSuper = true)
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
public class Laptop extends ElectronicDevice<Laptop> {

    private int ramSizeInGB;
}

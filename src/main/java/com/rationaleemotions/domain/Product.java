package com.rationaleemotions.domain;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Getter
@Setter
@ToString
@Accessors(chain = true, fluent = true)
public class Product {

    @Id
    @EmbeddedId //When we mark an Id column using this, we refer to an embeddable class here.
    // The embeddable class should be Serializable and should also implement hashCode() and equals()
    private ProductKey productKey;

    private String productName;
}

package com.rationaleemotions.domain;

import jakarta.persistence.Entity;

import java.util.Objects;

@Entity
public class CacheDisabledBike extends CommonBikeAttribs<CacheDisabledBike>{
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CacheDisabledBike that = (CacheDisabledBike) o;
        return Objects.equals(id(), that.id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id());
    }
}

package com.rationaleemotions.domain;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Embeddable
public class Address {

    @Enumerated(EnumType.STRING)
    private AddressType type;

    private String doorNumber;
    private String building;
    private String streetName;
    private String city;

    public enum AddressType {
        RESIDENCE,
        OFFICE
    }
}

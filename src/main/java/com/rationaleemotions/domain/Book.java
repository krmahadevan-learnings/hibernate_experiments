package com.rationaleemotions.domain;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Embeddable
@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class Book {
    private String name;
    private String author;
}

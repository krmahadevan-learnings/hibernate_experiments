package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Set;

@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
@NamedEntityGraph(name = "course->colleges", attributeNodes = @NamedAttributeNode("colleges"))
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private CourseType courseType;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @ToString.Exclude
    private Set<College> colleges;

    public enum CourseType {
        PART_TIME,
        FULL_TIME
    }
}

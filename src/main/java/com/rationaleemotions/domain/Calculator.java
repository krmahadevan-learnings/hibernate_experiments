package com.rationaleemotions.domain;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString(callSuper = true)
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
public class Calculator extends ElectronicDevice<Calculator> {

    private boolean scientific;

}

package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // I am dealing with a timestamp. In DB, I can save date (or) time (or) both.
    // I use the Temporal annotation to tell DB what is it that I want it to save.
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "started_at")
    private ZonedDateTime startedAt;

    @Column(name = "zone_id")
    private ZoneId zoneId;
}

package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.UUID;

@Entity
@Table(name = "persons")
@Setter
@Getter
@ToString
@Accessors(fluent = true, chain = true)
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Embedded
    private Name name;

    private String city;

    public static Person newInstance(String first, String last, String city) {
        return new Person().name(new Name().firstName(first).lastName(last)).city(city);
    }

}

package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Accessors(fluent = true)
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SuppressWarnings("unchecked")
public abstract class Animal<T extends Animal<?>> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    protected Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Species species;

    public T id(Long id) {
        this.id = id;
        return (T) this;
    }

    public T name(String name) {
        this.name = name;
        return (T) this;
    }

    public T species(Species species) {
        this.species = species;
        return (T) this;
    }
}

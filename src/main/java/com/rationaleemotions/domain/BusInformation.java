package com.rationaleemotions.domain;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@Embeddable
public class BusInformation {
    private String regNumber;
    private String brand;
    private boolean multiAxle;
}

package com.rationaleemotions.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString(callSuper = true)
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
@Table(name = "cricket_bowler")
public class CricketBowler extends Player<CricketBowler> {

    @Column(name = "overs")
    private int oversBowled;

    @Column(name = "wickets")
    private int wicketsTaken;
}

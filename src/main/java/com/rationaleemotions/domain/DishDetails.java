package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * This embeddable POJO holds the many to many relationship entity within it.
 * The entity that is going to embed this POJO into it would need to override the attributes or the association
 * if it wants to customize either the table columns or the JOIN Table
 */
@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Embeddable
public class DishDetails {

    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<Dish> dishes;
}

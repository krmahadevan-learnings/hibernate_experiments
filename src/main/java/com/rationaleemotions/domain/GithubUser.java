package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@Entity
@Table(name = "github_user")
public class GithubUser {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @ElementCollection //Indicates that we would like to access this data member as a collection (List|Set|Map)
    @CollectionTable(
            name = "github_repo", //"that" table which contains data that is to be transformed into a collection.
            joinColumns = {
                    @JoinColumn(name = "belongs_to") // "that" table's foreign key
            }
    )
    @Column(name = "repository_name") // The column name in "that" table which is to be collated as a collection.
    private List<String> repositories;
}

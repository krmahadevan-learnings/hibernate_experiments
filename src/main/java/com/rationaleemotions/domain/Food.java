package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@ToString
@Getter
@Setter
@Accessors(fluent = true, chain = true)
@SecondaryTable(
        name = "taste", // The name of "that" table.
        pkJoinColumns = @PrimaryKeyJoinColumn(name = "taste_id"),// This is the foreign key
        // available in "that" table using which a record
        //from both this and "that" table should be merged and mapped into this current entity.
        foreignKey = @ForeignKey(name = "food_taste") // This is the name that we are giving to the
        // foreign key relation that we created. This will ensure that our FK has a proper name
)
// Imagine the secondary table to be something like JPA's answer to dealing with vertical partitioning?
// At-least that's what I am telling myself as, to remember this concept of secondary tables.
// * Vertical partition ==> A table with say 100 columns is divided vertically into say 2 tables,
// each having 75 and 25 columns respectively.
// * Why do we do vertical partitioning?
// Well, we can perhaps move all heavy columns to the "other" table so that we load it only when we need it.
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private boolean vegetarian;

    @Enumerated(value = EnumType.STRING)
    private MealType mealType;

    private boolean hasOffer;

    private float price;

    @Column(table = "taste")
    private boolean tangy;
    @Column(table = "taste")
    private boolean spicy;
    @Column(table = "taste")
    private boolean sweet;
    @Column(table = "taste")
    private boolean bland;

    public enum MealType {
        STARTER,
        MAIN_COURSE,
        DESSERT
    }
}

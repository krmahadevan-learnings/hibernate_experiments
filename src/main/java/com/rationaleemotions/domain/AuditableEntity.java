package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@MappedSuperclass
@ToString
@Getter
@Setter
public abstract class AuditableEntity {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private LocalDateTime creationTime;

    @Column(name = "last_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime modificationTime;

    @PrePersist
    protected void updateDateCreated() {
        this.creationTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void updateLastModified() {
        this.modificationTime = LocalDateTime.now();
    }
}

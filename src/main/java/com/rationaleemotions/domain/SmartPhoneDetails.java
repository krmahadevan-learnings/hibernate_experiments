package com.rationaleemotions.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Embeddable;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * This Embeddable POJO embeds a Many to one relationship, which when embedded into an entity, will be used to
 * override the attributes and also the relations
 */
@Embeddable
@Getter
@Setter
@ToString
@Accessors(chain = true, fluent = true)
public class SmartPhoneDetails {

    @ManyToOne(cascade = CascadeType.PERSIST)
    private SmartPhone smartPhone;

    private String ownerName;

    private String mobileNumber;
}

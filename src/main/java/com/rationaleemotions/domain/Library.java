package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;

@ToString
@Getter
@Setter
@Accessors(fluent = true,chain = true)
@Entity
public class Library {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String city;

    @ElementCollection //Indicates that we would like to access this data member as a collection (List|Set|Map)
    @CollectionTable(
            name = "book", //"that" table which contains data that is to be transformed into a collection.
            joinColumns = {
                    @JoinColumn(name = "available_in"), // "that" table's foreign key
            }

    )
    @MapKeyColumn(name = "book_type") // "That" table's column value which is to be transformed into the map key (enum in this case)
    @MapKeyEnumerated(EnumType.STRING) //Because the key of our map is an enum
    private Map<BookType, Book> books;
}

package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@Accessors(fluent = true, chain = true)
@Entity
@NamedEntityGraph(
        name = Vehicle.GRAPH_NAME, // This is the name we are going to give to our graph. We need this when querying
        attributeNodes = @NamedAttributeNode("parts") // All the data members that are to be fetched eagerly
)
//This is our answer to the N+1 query problem. The problem occurs when someone basically queries all the vehicles.
// Since our fetch mode for the "OneToMany" is by default lazy, we end up generating an additional query to the
// "VehiclePart" table to get the details and thus causing the famous N+1 queries problem.
// It can be solved in a few ways.
//1. Change "fetch" to "EAGER" in the "OneToMany" annotation. This will cause the child entities to be loaded eagerly all the time.
//2. Use a named graph as shown above. So the fetch type toggles over to "eager" (internally it's a join), only when we
// query using the graph along with a map that contains reference to the named entity graph.
//3. If you are using a framework such as spring, you can additionally configure your JPA implementor telling how
// you would like it to handle those attributes which aren't referenced via the "attributeNodes" attribute.
// "Fetch" indicates, use eager for all mentioned attributes and default to "lazy" for all unmentioned attributes.
// "Load" indicate, use eager for all mentioned attributes and fall back to default fetch mode as applicable for the unmentioned attributes.

// Read more about N+1 problem in
// 1. https://hackernoon.com/3-ways-to-deal-with-hibernate-n1-problem
// 2. https://medium.com/geekculture/resolve-hibernate-n-1-problem-f0e049e689ab
// 3. https://vladmihalcea.com/jpa-entity-graph/

//Why can't I always go with "fetch" as "eager" to circumvent the N+1 problem ?

//1. Annotations are static and cannot be changed at runtime. So if you set "fetch" attribute of "OneToMany" to "EAGER"
// then by default it will always be eager.
//2. I want it to be lazy all the time, but only in some situations i want it to be fetched eagerly. This use-case
//cannot be solved by the annotation driven way.
public class Vehicle {

    public static final String GRAPH_NAME = "my_graph";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String brand;

    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;

    private String model;

    @OneToMany(mappedBy = "vehicle", // Represents the field in "that" entity, which holds our reference.
            // Internally this tells the JPA implementor, who is the owner of the relation
            // ( i.e., which table will have the foreign key)
            cascade = {CascadeType.PERSIST} //When this entity gets saved, ensure that you save "that" entity too.
    )
    private Set<VehiclePart> parts;

    public enum VehicleType {
        TWO_WHEELER,
        FOUR_WHEELER,
        SIX_WHEELER
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(id, vehicle.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    //Let's have an explicit toString() implementation that excludes the parts reference
    public String asString() {
        return "Vehicle{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", vehicleType=" + vehicleType +
                ", model='" + model + '\'' +
                '}';
    }
}

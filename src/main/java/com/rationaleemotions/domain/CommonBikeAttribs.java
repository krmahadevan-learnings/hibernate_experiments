package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Accessors(fluent = true, chain = true)
@MappedSuperclass
@SuppressWarnings("unchecked")
public class CommonBikeAttribs<T extends CommonBikeAttribs<?>> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String make;
    private String model;

    public T id(Long id) {
        this.id = id;
        return (T) this;
    }

    public T make(String make) {
        this.make = make;
        return (T) this;
    }

    public T model(String model) {
        this.model = model;
        return (T) this;
    }


}

package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Map;

@Entity
@Table(name = "tech_lead") //When our class name differs from the table name.
@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class TechnicalLead {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToMany(mappedBy = "lead") // "mappedBy" Represents the field in "that" entity, which holds our reference.
    // Internally this tells the JPA implementor, who is the owner of the relation
    // ( i.e., which table will have the foreign key)
    @MapKeyColumn(name = "id") //Represents the data member from "that" table entity, that should be used for the key.
    private Map<Long, SoftwareEngineer> engineers;

}

package com.rationaleemotions.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString(callSuper = true)
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
@Table(name = "football_player")
public class FootballPlayer extends Player<FootballPlayer> {

    @Column(name = "forward")
    private boolean isForward;
    @Column(name = "defence")
    private boolean isDefence;
    @Column(name = "goals")
    private int goalsScored;
}

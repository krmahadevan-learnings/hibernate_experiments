package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "engineer")
@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class SoftwareEngineer {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "which_lead") //We use this to explicitly name the column name of the foreign key
    // if it's name is NOT the default naming convention
    private TechnicalLead lead;

    @Override
    public String toString() {
        return "SoftwareEngineer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lead=(" + lead.id() + ", " + lead.name() + ")" +
                '}';
    }
}

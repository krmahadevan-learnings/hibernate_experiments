package com.rationaleemotions.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Entity
@Accessors(fluent = true, chain = true)
public class Bird extends Animal<Bird> {

    private String color;

    @Enumerated(EnumType.STRING)
    private BirdType type;

    public enum BirdType {
        MALLARD_DUCK,
        PIGEON,
        CANARY,
        COCKATOO,
        DOVE
    }
}

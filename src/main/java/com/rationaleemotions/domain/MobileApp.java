package com.rationaleemotions.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@Accessors(chain = true, fluent = true)
@Entity
@Table(name = "mobile_app")
public class MobileApp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(name = "developer_name")
    private String developerName;

    @Enumerated(EnumType.STRING)
    private MobilePlatform platform;

    @Enumerated(EnumType.STRING)
    @Column(name = "app_category")
    private AppCategory appCategory;

    //The field "ownerName" that is part of the embeddable is being overridden such that it now matches
    //with the column "owner_name" in the table "mobile_app
    @AttributeOverride(name = "ownerName", column = @Column(name = "owner_name"))
    //The field "mobileNumber" that is part of the embeddable is being overridden such that it now matches
    //with the column "owner_name" in the table "mobile_app
    @AttributeOverride(name = "mobileNumber", column = @Column(name = "mobile_number"))
    //We have a ManyToOne relationship created via the data member "smartPhone" in the embeddable object.
    //We are now overriding the association such that it now refers to the join column of our table "phone_id"
    // instead of defaulting to the default join column name which would be "smartphone_mobileapp_id"
    //We created the foreign key relationship via our "create table" wherein we specify the foreign key explicitly
    //whose SQL you can see on the console, when you run the test
    @AssociationOverride(name = "smartPhone", joinColumns = {@JoinColumn(name = "phone_id")})
    @Embedded
    private SmartPhoneDetails smartPhoneDetails;

    public enum AppCategory {
        PRODUCTIVITY,
        ENTERTAINMENT,
        GAMES
    }
}

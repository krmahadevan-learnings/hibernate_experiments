package com.rationaleemotions.domain;

import jakarta.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Embeddable
@Getter
@Setter
@ToString
@Accessors(chain = true, fluent = true)
@EqualsAndHashCode
public class ProductKey implements Serializable {

    private String batchName;
    private int batchId;
}

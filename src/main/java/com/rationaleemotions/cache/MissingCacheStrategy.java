package com.rationaleemotions.cache;

public enum MissingCacheStrategy {
    FAIL("fail"),

    CREATE_WARN("create-warn"),
    CREATE("create");

    private final String strategy;

    MissingCacheStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String text() {
        return this.strategy;
    }


}

package com.rationaleemotions.cache;

import lombok.Getter;
import lombok.experimental.Accessors;

public enum CacheRegionFactoryClass {

    HAZELCAST("jcache", "com.hazelcast.cache.HazelcastCachingProvider"),
    EH_CACHE("jcache", "org.ehcache.jsr107.EhcacheCachingProvider"),
    CAFFEINE("jcache", "com.github.benmanes.caffeine.jcache.spi.CaffeineCachingProvider"),
    INFINISPAN("infinispan", "org.infinispan.jcache.embedded.JCachingProvider");

    @Getter
    @Accessors(fluent = true)
    private final String provider;
    private final String name;
    CacheRegionFactoryClass(String name, String provider) {
        this.name = name;
        this.provider = provider;
    }

    public String text() {
        return this.name;
    }
}

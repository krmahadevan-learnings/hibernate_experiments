package com.rationaleemotions.persistence;

import com.rationaleemotions.cache.CacheRegionFactoryClass;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.spi.PersistenceUnitInfo;
import lombok.experimental.UtilityClass;
import org.hibernate.jpa.HibernatePersistenceProvider;

import java.util.Collections;
import java.util.Map;

@UtilityClass
public class HibernateUtils {

    public static EntityManagerFactory cacheAwareNewInstance(
            CacheRegionFactoryClass cacheRegionFactoryClass,
            Strategy strategy, Class<?>... classes) {
        return create(new CacheEnabledPersistenceUnitInfo(cacheRegionFactoryClass, strategy, classes));
    }

    public static EntityManagerFactory newInstance(Strategy strategy, Class<?>... classes) {
        return create(new CacheAgnosticPersistenceUnitInfo(strategy, classes));
    }

    private static EntityManagerFactory create(PersistenceUnitInfo info) {
        return new HibernatePersistenceProvider().createContainerEntityManagerFactory(info, Collections.emptyMap());

    }

    public static Map<String, Object> newGraph(EntityManager em, String graphName) {
        return Map.of(Constants.FETCH_GRAPH, em.getEntityGraph(graphName));
    }
}

package com.rationaleemotions.persistence;

import com.p6spy.engine.spy.P6DataSource;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.persistence.SharedCacheMode;
import jakarta.persistence.ValidationMode;
import jakarta.persistence.spi.ClassTransformer;
import jakarta.persistence.spi.PersistenceUnitInfo;
import jakarta.persistence.spi.PersistenceUnitTransactionType;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.postgresql.Driver;

import javax.sql.DataSource;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class CacheAgnosticPersistenceUnitInfo implements PersistenceUnitInfo {

    private final String strategy;

    private final List<String> classes;

    public CacheAgnosticPersistenceUnitInfo(Strategy strategy, Class<?>... classes) {
        this.strategy = strategy.text();
        this.classes = Arrays.stream(classes).map(Class::getName).collect(Collectors.toList());
    }

    @Override
    public String getPersistenceUnitName() {
        return "dragon-warrior";
    }

    @Override
    public String getPersistenceProviderClassName() {
        return HibernatePersistenceProvider.class.getName();
    }

    @Override
    public PersistenceUnitTransactionType getTransactionType() {
        return PersistenceUnitTransactionType.RESOURCE_LOCAL;
    }

    @Override
    public DataSource getJtaDataSource() {
        return logAwareDataSource();
    }

    @Override
    public DataSource getNonJtaDataSource() {
        return logAwareDataSource();
    }

    private static DataSource logAwareDataSource() {
        return new P6DataSource(newDataSource());
    }

    private static volatile HikariDataSource ds;

    public static DataSource newDataSource() {
        if (ds != null) {
            return ds;
        }
        synchronized (DataSource.class) {
            if (ds == null) {
                synchronized (DataSource.class) {
                    ds = new HikariDataSource();
                    ds.setUsername("postgres");
                    ds.setPassword("postgres");
                    ds.setJdbcUrl("jdbc:postgresql://localhost:5432/inventory");
                    ds.setDriverClassName(Driver.class.getName());
                }
            }
        }
        return ds;
    }

    @Override
    public Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("jakarta.persistence.schema-generation.database.action", strategy);
        return properties;
    }

    @Override
    public List<String> getManagedClassNames() {
        return classes;
    }


    @Override
    public List<String> getMappingFileNames() {
        return null;
    }

    @Override
    public List<URL> getJarFileUrls() {
        return null;
    }

    @Override
    public URL getPersistenceUnitRootUrl() {
        return null;
    }

    @Override
    public boolean excludeUnlistedClasses() {
        return false;
    }

    @Override
    public SharedCacheMode getSharedCacheMode() {
        return null;
    }

    @Override
    public ValidationMode getValidationMode() {
        return null;
    }

    @Override
    public String getPersistenceXMLSchemaVersion() {
        return null;
    }

    @Override
    public ClassLoader getClassLoader() {
        return null;
    }

    @Override
    public void addTransformer(ClassTransformer transformer) {
    }

    @Override
    public ClassLoader getNewTempClassLoader() {
        return null;
    }
}

package com.rationaleemotions.persistence;

public enum Strategy {

    NONE("none"),
    CREATE("create"),
    UPDATE("update"), //Hibernate knows this but JPA specs dont know about this.
    DROP("drop"),
    DROP_AND_CREATE("drop-and-create");

    private final String strategy;

    Strategy(String strategy) {
        this.strategy = strategy;
    }

    public String text() {
        return this.strategy;
    }
}

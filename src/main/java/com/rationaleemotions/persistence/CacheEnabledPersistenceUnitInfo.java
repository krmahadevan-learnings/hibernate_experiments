package com.rationaleemotions.persistence;

import com.rationaleemotions.cache.CacheRegionFactoryClass;
import com.rationaleemotions.cache.MissingCacheStrategy;
import jakarta.persistence.SharedCacheMode;

import java.util.Properties;

public class CacheEnabledPersistenceUnitInfo extends CacheAgnosticPersistenceUnitInfo {

    private final CacheRegionFactoryClass cacheRegionFactoryClass;
    public CacheEnabledPersistenceUnitInfo(CacheRegionFactoryClass cacheRegionFactoryClass,
                                                   Strategy strategy, Class<?>... classes) {
        super(strategy, classes);
        this.cacheRegionFactoryClass = cacheRegionFactoryClass;
    }

    @Override
    public Properties getProperties() {

        Properties properties = super.getProperties();
        //This generates a lot of statistics on the console which can be reviewed.
        properties.setProperty("hibernate.generate_statistics", "true");
        // Use Infinispan second level cache provider
        // Refer https://infinispan.org/docs/stable/titles/hibernate/hibernate.html#single_node_standalone_hibernate_application
        properties.setProperty("hibernate.cache.region.factory_class", cacheRegionFactoryClass.text());
        //Enabling second-level caches
        properties.setProperty("hibernate.cache.use_second_level_cache", "true");

        //Indicates what should hibernate do, if in case it finds a cache to be missing.
        //Read more at https://docs.jboss.org/hibernate/orm/6.1/userguide/html_single/Hibernate_User_Guide.html#caching-provider-jcache-missing-cache-strategy
        properties.setProperty("hibernate.javax.cache.missing_cache_strategy", MissingCacheStrategy.CREATE_WARN.text());

        return properties;
    }

    @Override
    public SharedCacheMode getSharedCacheMode() {
        //For detailed explanation refer https://docs.jboss.org/hibernate/orm/6.1/userguide/html_single/Hibernate_User_Guide.html#caching-mappings
        //ENABLE_SELECTIVE indicates that we should NOT cache entities UNLESS and UNTIL they are annotated using @Cacheable
        return SharedCacheMode.ENABLE_SELECTIVE;
    }
}

package com.rationaleemotions;

import com.rationaleemotions.domain.Vehicle;
import com.rationaleemotions.domain.VehiclePart;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class OneToManyRelationshipDemoTest {

    @Test
    public void showOneToManyRelationship() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Vehicle.class, VehiclePart.class)) {
            long key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Vehicle miniCooper = new Vehicle()
                        .vehicleType(Vehicle.VehicleType.FOUR_WHEELER)
                        .brand("Mini")
                        .model("Countryman");
                List<VehiclePart> parts = Arrays.asList(
                        new VehiclePart().vehicle(miniCooper).serialNumber("abc1").name("music-system"),
                        new VehiclePart().vehicle(miniCooper).serialNumber("abc1").name("music-system")
                );
                miniCooper.parts(new HashSet<>(parts));
                em.persist(miniCooper);
                tx.commit();
                key = miniCooper.id();
            }


            try (EntityManager em = emf.createEntityManager()) {
                Vehicle found = em.find(Vehicle.class, key, HibernateUtils.newGraph(em, Vehicle.GRAPH_NAME));
                System.err.println(found.asString());
                found.parts().forEach(System.err::println);
            }
        }
    }
}

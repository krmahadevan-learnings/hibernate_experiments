package com.rationaleemotions;

import com.rationaleemotions.domain.*;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.UUID;

public class PrimaryKeyDemoTest {

    @Test
    public void usingIdClass() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Student.class)) {
            Name key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Student s1 = new Student()
                        .firstName("Krishnan")
                        .lastName("Mahadevan")
                        .rollNumber(1);
                em.persist(s1);
                tx.commit();
                key = new Name().firstName(s1.firstName()).lastName(s1.lastName());
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Student.class, key));
            }
        }
    }

    @Test
    public void usingEmbeddedCompositeKey() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Product.class)) {
            ProductKey key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Product p1 = new Product()
                        .productKey(
                                new ProductKey().batchId(1).batchName("bengaluru")
                        ).productName("tobacco");
                em.persist(p1);
                key = p1.productKey();
                tx.commit();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Product.class, key));
            }
        }
    }

    @Test
    public void usingCustomKeyGenerator() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Employee.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Employee employee = new Employee().address("Some-address").name("Krishnan");
                em.persist(employee);
                tx.commit();
                UUID uid = employee.id();
                System.err.println(em.find(Employee.class, uid));
                System.err.println("Time = " + uid.timestamp());
            }
        }
    }

}

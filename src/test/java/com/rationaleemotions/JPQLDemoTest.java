package com.rationaleemotions;

import com.rationaleemotions.domain.Directory;
import com.rationaleemotions.domain.Vehicle;
import com.rationaleemotions.domain.VehiclePart;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class JPQLDemoTest {

    @Test
    public void runDemo() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Directory.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                Long value = (Long) em.createNativeQuery("SELECT last_value from directory_id_seq;", Long.class).getSingleResult();
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                AtomicInteger counter = new AtomicInteger(value.intValue());
                IntStream.rangeClosed(1, 100)
                        .forEach(it -> {
                            Directory directory = new Directory().name("File_" + counter.getAndIncrement());
                            em.persist(directory);
                        });
                tx.commit();
            }

            System.err.println("Printing ONLY 1 record");
            try (EntityManager em = emf.createEntityManager()) {
                TypedQuery<Directory> query = em.createQuery("select d from Directory  d", Directory.class);
                query.setMaxResults(1);
                query.getResultList().forEach(System.err::println);
            }

            System.err.println("Printing ONLY first 20 record(s)");
            try (EntityManager em = emf.createEntityManager()) {
                em.createQuery("select d from Directory d where d.id <= 50", Directory.class)
                        .setMaxResults(20)
                        .getResultList()
                        .forEach(System.err::println);
            }
        }
    }

    @Test(dependsOnMethods = "runDemo")
    public void demoNamedQueries() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, Directory.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                System.err.println("Printing First 10 Records");
                em.createNamedQuery(Directory.DIRECTORY_FIRST10, Directory.class).getResultList()
                        .forEach(System.err::println);
                System.err.println("Printing Last 10 Records");
                em.createNamedQuery(Directory.DIRECTORY_LAST10, Directory.class).getResultList()
                        .forEach(System.err::println);
            }
        }
    }

    @Test
    public void demoJoinsUsingJPQL() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Vehicle.class, VehiclePart.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Vehicle miniCooper = new Vehicle()
                        .vehicleType(Vehicle.VehicleType.FOUR_WHEELER)
                        .brand("Mini")
                        .model("Countryman");
                List<VehiclePart> parts = Arrays.asList(
                        new VehiclePart().vehicle(miniCooper).serialNumber("abc1").name("music-system"),
                        new VehiclePart().vehicle(miniCooper).serialNumber("abc1").name("music-system")
                );
                miniCooper.parts(new HashSet<>(parts));
                Vehicle maruti = new Vehicle()
                        .vehicleType(Vehicle.VehicleType.FOUR_WHEELER)
                        .brand("Maruti")
                        .model("Swift");
                List<VehiclePart> marutiParts = Arrays.asList(
                        new VehiclePart().vehicle(maruti).serialNumber("abc1").name("music-system"),
                        new VehiclePart().vehicle(maruti).serialNumber("abc1").name("music-system")
                );
                maruti.parts(new HashSet<>(marutiParts));
                em.persist(maruti);
                tx.commit();
            }


            String query = "Select v from Vehicle v, VehiclePart vp where vp.vehicle = v and v.model = :model";
            try (EntityManager em = emf.createEntityManager()) {
                Vehicle found = em.createQuery(query, Vehicle.class).setParameter("model", "Swift")
                        .getSingleResult();
                System.err.println(found.asString());
                found.parts().forEach(System.err::println);
            }
        }
    }
}

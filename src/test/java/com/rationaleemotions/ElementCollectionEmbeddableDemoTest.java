package com.rationaleemotions;

import com.rationaleemotions.domain.BusDepot;
import com.rationaleemotions.domain.BusInformation;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.List;

public class ElementCollectionEmbeddableDemoTest extends ManualSchemaSetupHelper {

    @Test
    public void showElementCollection() {
        //This sample expects the required tables/sequences/relationships to be pre-created
        // Hence we are passing the strategy as NONE
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, BusDepot.class)) {
            long key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                List<BusInformation> buses = List.of(
                        new BusInformation().brand("Ashok Leyland").multiAxle(false).regNumber("KA-01-1234"),
                        new BusInformation().brand("Volvo").multiAxle(true).regNumber("KA-51-9876")
                );
                BusDepot depot = new BusDepot()
                        .location("Majestic")
                        .supervisor("Jack Daniel")
                        .buses(buses);
                em.persist(depot);
                tx.commit();
                key = depot.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(BusDepot.class, key));
            }

        }
    }

    @Override
    public String[] queries() {
        return new String[]{
                """
                    create table public.bus_depot (
                        id serial NOT NULL PRIMARY KEY,
                        location varchar(255) NOT NULL,
                        supervisor varchar(255) NOT NULL
                    );
                """,
                """
                    create table public.bus (
                        registration_number varchar(255) NOT NULL,
                        brand varchar(255) NOT NULL,
                        multi_axle boolean NOT NULL,
                        belongs_to bigInt NOT NULL,
                        FOREIGN KEY (belongs_to) REFERENCES public.bus_depot(id)
                    );
                """
        };
    }

    @Override
    public String[] sequences() {
        return new String[]{"bus_depot_seq"};
    }

    @Override
    public String[] tables() {
        return new String[]{"public.bus", "public.bus_depot"};
    }

    @Override
    public boolean runTableSetup() {
        return true;
    }

}

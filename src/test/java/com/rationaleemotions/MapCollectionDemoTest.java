package com.rationaleemotions;

import com.rationaleemotions.domain.Book;
import com.rationaleemotions.domain.BookType;
import com.rationaleemotions.domain.Library;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.Map;

public class MapCollectionDemoTest extends ManualSchemaSetupHelper {

    @Test
    public void demoMapCollection() {
        //This sample expects the required tables/sequences/relationships to be pre-created
        // Hence we are passing the strategy as NONE
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, Library.class)) {
            Long id;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();

                Map<BookType, Book> books = Map.of(
                        BookType.AUDIOBOOK, new Book().name("Audio adventures").author("Old Monk"),
                        BookType.PAPERBACK, new Book().name("Paper adventures").author("Smirnoff"),
                        BookType.DIGITAL_PDF, new Book().name("PDF adventures").author("Grey Goose")
                );

                Library library = new Library().books(books).city("Bengaluru");
                em.persist(library);
                tx.commit();
                id = library.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Library.class, id));
            }
        }
    }

    @Override
    public boolean runTableSetup() {
        return true;
    }

    @Override
    public String[] queries() {
        return new String[]{
                """
                create table public.library (
                    id serial NOT NULL PRIMARY KEY,
                    city varchar(100) NOT NULL
                );
                """,
                """
                create table public.book (
                    book_type varchar(20) NOT NULL,
                    name varchar(255) NOT NULL,
                    author varchar(255) NOT NULL,
                    available_in bigint NOT NULL,
                    FOREIGN KEY (available_in) REFERENCES public.library(id)
                );
                """
        };
    }

    @Override
    public String[] sequences() {
        return new String[]{"library_seq"};
    }

    @Override
    public String[] tables() {
        return new String[]{"public.library", "public.book"};
    }
}

package com.rationaleemotions;

import com.rationaleemotions.persistence.CacheAgnosticPersistenceUnitInfo;
import org.testng.annotations.BeforeClass;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class ManualSchemaSetupHelper {

    public abstract String[] queries();

    public abstract boolean runTableSetup();

    public abstract  String[] sequences();

    public abstract String[] tables();

    @BeforeClass
    public void setup() throws SQLException {
        if (!runTableSetup()) {
            return;
        }

        DataSource ds = CacheAgnosticPersistenceUnitInfo.newDataSource();

        try (Connection cn = ds.getConnection()) {
            Statement statement = cn.createStatement();
            for (String seq: sequences()) {
                String sql = "drop sequence if exists " + seq + ";";
                statement.execute(sql);
                System.err.println("[Sequence Drop]==>" + sql);
                sql = "create sequence " + seq + " increment by 50;";
                statement.execute(sql);
                System.err.println("[Sequence Creation]==>" + sql);
            }
            for (String table: tables()) {
                String sql = "drop table if exists " + table + " cascade;";
                statement.execute(sql);
                System.err.println("[Table Drop]===>" + sql);
            }
            for (String query : queries()) {
                statement.execute(query);
                System.err.println("[Query exec]===>" + query);
            }
        }

    }
}

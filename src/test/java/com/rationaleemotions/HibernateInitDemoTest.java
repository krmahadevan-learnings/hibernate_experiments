package com.rationaleemotions;

import com.rationaleemotions.domain.Name;
import com.rationaleemotions.domain.Person;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.testng.annotations.Test;

import java.util.Optional;
import java.util.UUID;

public class HibernateInitDemoTest {

    private UUID key;

    @Test
    public void usingXml() {
        try (EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpa-demo-local")) {
            EntityManager em = factory.createEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Person person = Person.newInstance("Krishnan", "Mahadevan-" + UUID.randomUUID(), "Bangalore");
            em.persist(person);
            em.createQuery("select p from Person p", Person.class).getResultList()
                    .forEach(p -> System.err.println("Person ==>" + p));
            System.err.println("Printing the id " + person.id());
            key = person.id();
            tx.commit();
        }
    }

    @Test(dependsOnMethods = "usingXml")
    public void viaCode() {
        try (EntityManagerFactory factory = HibernateUtils.newInstance(Strategy.UPDATE, Person.class)) {
            try (EntityManager em = factory.createEntityManager()) {

                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Person person = em.find(Person.class, key);
                Optional.ofNullable(person)
                        .ifPresent(
                                found -> {
                                    person.name(new Name().firstName("Dragon").lastName("Warrior"));
//            Person person = Person.newInstance("Krishnan", "Mahadevan-" + UUID.randomUUID(), "Bangalore");
//            em.persist(person);
                                    em.createQuery("select p from Person p", Person.class).getResultList()
                                            .forEach(p -> System.err.println("Person ==>" + p));
//            System.err.println("Printing the id " + person.getId());
                                    em.detach(person);

                                }
                        );

                Person newPerson = Person.newInstance("Dragon", "Warrior", "Bangalore");
                tx.commit();
            }
        }
    }
}

package com.rationaleemotions;

import com.rationaleemotions.domain.Event;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.time.ZonedDateTime;

public class TemporalDemoTest {

    @Test
    public void showTemporal() {
        //drop-and-create
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Event.class)) {
            Long key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                ZonedDateTime time = ZonedDateTime.now();
                Event event = new Event().startedAt(time).zoneId(time.getZone());
                em.persist(event);
                tx.commit();
                key = event.id();
            }
            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Event.class, key));
            }
        }
    }
}

package com.rationaleemotions;

import com.rationaleemotions.domain.Certification;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public class HibernateListenersDemoTest {

    @Test
    public void runDemo() throws InterruptedException {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Certification.class)) {
            long key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Certification certification = new Certification()
                        .certificationDate(LocalDateTime.now())
                        .certificationIssuer("TestNG Community")
                        .validUntil(LocalDateTime.now().plusYears(5))
                        .courseName("Basics of TestNG");
                em.persist(certification);
                tx.commit();
                key = certification.id();
            }

            System.err.println("Printing before Modification");
            try (EntityManager em = emf.createEntityManager()) {
                Certification found = em.find(Certification.class, key);
                Assert.assertNotNull(found.getCreationTime(), "We should have had a valid creation time");
                Assert.assertNull(found.getModificationTime(), "We should NOT have had any modification time");
                System.err.println(found);
            }

            TimeUnit.SECONDS.sleep(10);

            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Certification found = em.find(Certification.class, key);
                found.certificationIssuer("TestNG Academy");
                tx.commit();
            }

            System.err.println("Printing after Modification");
            try (EntityManager em = emf.createEntityManager()) {
                Certification found = em.find(Certification.class, key);
                Assert.assertNotNull(found.getModificationTime(), "We should NOT have had any modification time");
                long elapsedSeconds = found.getCreationTime().until(found.getModificationTime(), ChronoUnit.SECONDS);
                Assert.assertTrue(elapsedSeconds >= 10, "There should have been atleast 10 second delay");
                System.err.println(found);
            }

        }
    }
}

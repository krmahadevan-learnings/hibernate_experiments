package com.rationaleemotions;

import com.rationaleemotions.domain.Person;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

public class GeneratedValueDemoTest {
    @Test
    public void runDemo() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Person.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Person person = Person.newInstance("Krishnan", "Mahadevan", "Bangalore");
                em.persist(person);
                em.persist(Person.newInstance("Bhuvaneshwari", "Mahadevan", "Bangalore"));
                tx.commit();
            }
        }
    }
}

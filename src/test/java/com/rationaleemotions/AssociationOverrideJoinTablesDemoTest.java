package com.rationaleemotions;

import com.rationaleemotions.domain.*;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.Arrays;

public class AssociationOverrideJoinTablesDemoTest extends ManualSchemaSetupHelper {
    @Test
    public void overrideAssociationViaJoinTable() {
        //This sample expects the required tables/sequences/relationships to be pre-created
        //Hence we are passing the strategy as NONE
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, Hotel.class, Dish.class)) {
            Long id;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Dish parotta = new Dish().name("parotta");
                Dish redChutney = new Dish().name("red chutney");
                DishDetails dishDetails = new DishDetails().dishes(Arrays.asList(parotta, redChutney));
                Hotel hotel = new Hotel().name("Empire").dishDetails(dishDetails);
                em.persist(hotel);
                tx.commit();
                id = hotel.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Hotel.class, id));
            }
        }
    }

    @Override
    public String[] sequences() {
        return new String[]{"hotel_seq", "dish_seq"};
    }

    @Override
    public String[] tables() {
        return new String[]{"public.hotel", "public.dish", "public.hotels_and_dishes"};
    }

    @Override
    public boolean runTableSetup() {
        return true;
    }


    @Override
    public String[] queries() {
        return new String[]{
                "create table public.hotel (id serial NOT NULL PRIMARY KEY,name varchar(250) NOT NULL);",
                "create table public.dish (id serial NOT NULL PRIMARY KEY,name varchar(250) NOT NULL);",
                """
                    create table public.hotels_and_dishes(
                        dish bigint NOT NULL,
                        hotel bigint NOT NULL,
                        FOREIGN KEY(dish) REFERENCES public.dish(id),
                        FOREIGN KEY(hotel) REFERENCES public.hotel(id)
                    );
                """
        };
    }

}

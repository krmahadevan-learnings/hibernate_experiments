package com.rationaleemotions;

import com.rationaleemotions.domain.College;
import com.rationaleemotions.domain.Course;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.Set;

public class ManyToManyRelationshipDemoTest {

    @Test
    public void showOneToManyRelationship() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, College.class, Course.class)) {
            long vasaviKey;
            long csKey;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();

                Course cs = new Course().name("Computer Science").courseType(Course.CourseType.FULL_TIME);
                Course arts = new Course().name("Literature").courseType(Course.CourseType.FULL_TIME);
                Course ai = new Course().name("Artificial Intelligence").courseType(Course.CourseType.FULL_TIME);

                College vasavi = new College().courses(Set.of(cs, arts)).type(College.CollegeType.ARTS_AND_SCIENCE)
                                .name("Vasavi College");
                College irtt = new College().courses(Set.of(cs, arts)).type(College.CollegeType.ARTS_AND_SCIENCE)
                        .name("Vasavi College");
                cs.colleges(Set.of(vasavi, irtt));
                arts.colleges(Set.of(vasavi));
                ai.colleges(Set.of(irtt));
                em.persist(vasavi);
                em.persist(irtt);
                tx.commit();
                vasaviKey = vasavi.id();
                csKey = cs.id();

            }

            System.err.println("Printing Computer Science course and colleges that offer it.");
            Course cs;
            try (EntityManager em = emf.createEntityManager()) {
                cs = em.find(Course.class, csKey);
                System.err.println(cs);
                cs.colleges().forEach(System.err::println);
            }

            System.err.println("Printing Vasavi college and the courses it offers");
            try (EntityManager em = emf.createEntityManager()) {
                College vasavi = em.find(College.class, vasaviKey);
                System.err.println(vasavi);
                vasavi.courses().forEach(System.err::println);
            }
        }
    }
}

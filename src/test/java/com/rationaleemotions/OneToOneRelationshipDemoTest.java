package com.rationaleemotions;

import com.rationaleemotions.domain.*;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

public class OneToOneRelationshipDemoTest {

    @Test
    public void showOneToOneRelationship() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Citizen.class, Passport.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Passport passport = new Passport().number("ABC123");
                Citizen citizen = new Citizen()
                        .name(new Name().firstName("Krishnan").lastName("Mahadevan"))
                        .passport(passport);
                passport.citizen(citizen);
                em.persist(passport);
                em.persist(citizen);
                tx.commit();
            }

            try (EntityManager em = emf.createEntityManager()) {
                em.createQuery("select c from Citizen c where c.passport.number = :passPortNumber", Citizen.class)
                        .setParameter("passPortNumber", "ABC123")
                        .getResultList()
                        .forEach(System.err::println);
                em.createQuery("select p from Passport  p where " +
                                "p.citizen.name.firstName = :first and p.citizen.name.lastName = :last", Passport.class)
                        .setParameter("first", "Krishnan")
                        .setParameter("last", "Mahadevan")
                        .getResultList()
                        .forEach(it -> {
                            System.err.println(it);
                            System.err.println("Citizen : " + it.citizen());
                        });
            }
            try (EntityManager em = emf.createEntityManager()) {
                em.createQuery("select p from Passport  p where " +
                                "p.citizen.name.firstName = :first and p.citizen.name.lastName = :last", Passport.class)
                        .setParameter("first", "Krishnan")
                        .setParameter("last", "Mahadevan")
                        .getResultList()
                        .forEach(it -> {
                            System.err.println(it);
                            System.err.println("Citizen : " + it.citizen());
                        });
            }
        }
    }

}

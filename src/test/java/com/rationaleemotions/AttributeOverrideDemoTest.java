package com.rationaleemotions;

import com.rationaleemotions.domain.*;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

public class AttributeOverrideDemoTest {

    @Test
    public void overrideAttribute() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Address.class, Company.class)) {
            Long id;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Company company = new Company().name("Mannar and Company")
                        .address(new Address().doorNumber("161").streetName("First Main").city("Bengaluru").type(Address.AddressType.OFFICE));
                em.persist(company);
                tx.commit();
                id = company.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Company.class, id));
            }
        }
    }
}

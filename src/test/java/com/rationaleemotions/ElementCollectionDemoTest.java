package com.rationaleemotions;

import com.rationaleemotions.domain.GithubUser;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.Arrays;

public class ElementCollectionDemoTest extends ManualSchemaSetupHelper {

    @Test
    public void showElementCollection() {
        //This sample expects the required tables/sequences/relationships to be pre-created
        // Hence we are passing the strategy as NONE
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, GithubUser.class)) {
            long key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                GithubUser user = new GithubUser()
                        .userName("krmahadevan")
                        .repositories(Arrays.asList("simplessh", "simplese"));
                em.persist(user);
                tx.commit();
                key = user.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(GithubUser.class, key));
            }

        }
    }

    @Override
    public boolean runTableSetup() {
        return true;
    }

    @Override
    public String[] queries() {
        return new String[]{
                "create table public.github_user (id serial NOT NULL PRIMARY KEY,user_name varchar(255) NOT NULL UNIQUE);",
                """
                    create table public.github_repo (
                        repository_name varchar(255) NOT NULL UNIQUE,
                        belongs_to bigInt not null,
                        FOREIGN KEY (belongs_to) REFERENCES public.github_user(id)
                    );
                """
        };
    }

    @Override
    public String[] tables() {
        return new String[]{"public.github_user", "public.github_repo"};
    }

    @Override
    public String[] sequences() {
        return new String[]{"github_user_seq"};
    }
}

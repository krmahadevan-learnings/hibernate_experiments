package com.rationaleemotions;

import com.rationaleemotions.domain.Name;
import com.rationaleemotions.domain.Person;
import com.rationaleemotions.domain.Product;
import com.rationaleemotions.domain.ProductKey;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.hibernate.LazyInitializationException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class EntityManagerCapabilitiesDemoTest {
    private UUID id;

    @Test
    public void demoMerge() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.UPDATE, Product.class)) {
            Product product;
            ProductKey productKey;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();

                tx.begin();
                product = new Product().productName("Noodles")
                        .productKey(new ProductKey().batchName(UUID.randomUUID().toString()).batchId(1001));
                em.persist(product);
                productKey = product.productKey();
                tx.commit();
            }

            //When we are here, we have a valid reference to a valid Product (via a Hibernate Proxy of-course)
            //but this product reference that we have is said to be "detached" because it is NOT having an active
            //Context
            try (EntityManager em = emf.createEntityManager()) {
                //So here we created a new Context, and then used merge() to add the detached object back
                //into the context
                product = em.merge(product);
                Assert.assertTrue(em.contains(product), "We should have had the DETACHED product back in the context");
            }

            //Now this is actually another use case, wherein we are going to be adding a totally new object
            //into the context, using the merge())
            try (EntityManager em = emf.createEntityManager()) {
                //We are creating a new product object,but it's referring to an existing record in the DB
                Product dummy = new Product().productKey(productKey);
                //When we do this, we have now gotten ourselves a managed "dummy" product
                dummy = em.merge(dummy);
                //That is why this assertion works all the time.
                Assert.assertTrue(em.contains(dummy), "We should have had the new DUMMY product back in the context");
            }
        }
    }

    @Test
    public void demoGetReference() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.UPDATE, Person.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Person person = new Person().name(new Name().firstName("Julius").lastName("Caesar")).city("Rome");
                em.persist(person);
                tx.commit();
                id = person.id();
            }
            Person reference;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                System.err.println("Fetching");
                reference = em.getReference(Person.class, id);
                System.err.println("Get Reference Employee " + reference);
                reference.name(new Name().firstName("Kung-fu").lastName("panda"));
                em.refresh(reference);
                tx.commit();
            }

        }
    }

    @Test(dependsOnMethods = "demoGetReference",
            description = "Remember to use an entity Proxy ONLY within a Context, when working with getReference()")
    public void ensureCannotAccessObjectOutsideContext() {
        Person reference;
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, Person.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                //This line will never trigger an SQL select statement but ONLY create a Hibernate Proxy object
                //which kind of acts like a placeholder for the Person object but without its values being set.
                reference = em.getReference(Person.class, id);
            }
        }
        try {
            //Now here we are triggering a toString() on the Person object which causes Hibernate to actually
            //try and fire a query. But since it does not have a valid context here (i.e., no active hibernate session
            //available via EntityManager, it will trigger an exception.
            //Remember, a query can be triggered by Hibernate only from within a valid Context (Session)
            System.err.println("Trying to print the reference entity outside a JPA Context: " + reference);
            Assert.fail("We should have never got here.");
        } catch (LazyInitializationException e) {
            System.err.println("Encountered the problem: " + e.getMessage());
        }
    }
}

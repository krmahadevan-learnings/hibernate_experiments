package com.rationaleemotions;

import com.rationaleemotions.cache.CacheRegionFactoryClass;
import com.rationaleemotions.domain.CacheDisabledBike;
import com.rationaleemotions.domain.CacheEnabledBike;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.Cache;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import javax.cache.Caching;

@Slf4j
public class JPACachingDemoTest {

    @Test
    public void runHazelcastDemo() {
        System.setProperty("hazelcast.jcache.provider.type", "server");
        runCacheDemo(CacheRegionFactoryClass.HAZELCAST);
    }
    @Test
    public void runCaffeineDemo() {
        runCacheDemo(CacheRegionFactoryClass.CAFFEINE);
    }
    @Test
    public void runEhCacheDemo() {
        runCacheDemo(CacheRegionFactoryClass.EH_CACHE);
    }

    @Test
    public void runInfiniSpanDemo() {
        runCacheDemo(CacheRegionFactoryClass.INFINISPAN);
    }

    private static void runCacheDemo(CacheRegionFactoryClass factoryClass) {
        try {
            Class<?> clazz = Class.forName(factoryClass.provider());
            if (!clazz.equals(Caching.getCachingProvider().getClass())) {
                throw new SkipException("Caching system does not match " + factoryClass.text());
            }
            log.warn("Running Cache Demo with Factory as {}", factoryClass.provider());
        } catch (ClassNotFoundException e) {
            throw new SkipException(factoryClass.provider() + " NOT Found in CLASSPATH. Skipping Execution");
        }
        try (EntityManagerFactory emf = HibernateUtils.cacheAwareNewInstance(
                factoryClass,
                Strategy.DROP_AND_CREATE, CacheEnabledBike.class, CacheDisabledBike.class)) {
            long cachedBikeId;
            long disabledCachedBikeId;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                CacheEnabledBike cachedBike = new CacheEnabledBike().make("Hero").model("Splendor");
                em.persist(cachedBike);
                CacheDisabledBike cacheDisabledBike = new CacheDisabledBike().make("TVS").model("Jupiter");
                em.persist(cacheDisabledBike);
                tx.commit();
                disabledCachedBikeId = cacheDisabledBike.id();
                cachedBikeId = cachedBike.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                Cache cache = emf.getCache();
                CacheEnabledBike cached = em.find(CacheEnabledBike.class, cachedBikeId);
                Assert.assertTrue(cache.contains(CacheEnabledBike.class, cached.id()),
                        CacheEnabledBike.class.getName() + " should HAVE BEEN cached.");
                CacheDisabledBike disabledCache = em.find(CacheDisabledBike.class, disabledCachedBikeId);
                Assert.assertFalse(cache.contains(CacheDisabledBike.class, disabledCache.id()),
                        CacheDisabledBike.class.getName() + " SHOULD NOT HAVE BEEN cached.");
            }
        }

    }
}

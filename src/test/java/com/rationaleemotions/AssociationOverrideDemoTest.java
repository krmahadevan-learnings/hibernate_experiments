package com.rationaleemotions;

import com.rationaleemotions.domain.MobileApp;
import com.rationaleemotions.domain.MobilePlatform;
import com.rationaleemotions.domain.SmartPhone;
import com.rationaleemotions.domain.SmartPhoneDetails;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

public class AssociationOverrideDemoTest extends ManualSchemaSetupHelper {

    @Test
    public void overrideAssociationViaJoinColumn() {
        //This sample expects the required tables/sequences/relationships to be pre-created
        //Hence we are passing the strategy as NONE
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, MobileApp.class, SmartPhone.class)) {
            Long id;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                SmartPhone phone = new SmartPhone()
                        .manufacturer("Apple")
                        .make("iPhone")
                        .model("13 PRO Max")
                        .mobilePlatform(MobilePlatform.IOS)
                        .imei("IMEI-1");
                SmartPhoneDetails details = new SmartPhoneDetails().smartPhone(phone).ownerName("Krishnan")
                        .mobileNumber("9841012345");
                MobileApp whatsapp = new MobileApp()
                        .smartPhoneDetails(details)
                        .name("Whatsapp")
                        .appCategory(MobileApp.AppCategory.PRODUCTIVITY)
                        .platform(MobilePlatform.IOS)
                        .developerName("FaceBook");
                MobileApp instagram = new MobileApp()
                        .smartPhoneDetails(details)
                        .name("Whatsapp")
                        .appCategory(MobileApp.AppCategory.PRODUCTIVITY)
                        .platform(MobilePlatform.IOS)
                        .developerName("FaceBook");
                em.persist(whatsapp);
                em.persist(instagram);
                tx.commit();
                id = whatsapp.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(MobileApp.class, id));
            }
        }
    }

    @Override
    public String[] queries() {
        return new String[]{
                """
                    create table public.smart_phone (
                            id serial NOT NULL PRIMARY KEY,
                            manufacturer varchar(250)  NOT NULL,
                            make varchar(50) NOT NULL,
                            model varchar(50) NOT NULL,
                            platform varchar (20) NOT NULL,
                            imei varchar(15) NOT NULL
                    );
                """,
                """
                    create table public.mobile_app (
                            id serial NOT NULL PRIMARY KEY,
                            name varchar(250) NOT NULL,
                            developer_name varchar(250) NOT NULL,
                            platform varchar (20) NOT NULL,
                            app_category varchar (20) NOT NULL,
                            owner_name varchar(250) NOT NULL,
                            mobile_number varchar(10) NOT NULL,
                            phone_id bigint not null,
                            FOREIGN KEY (phone_id) REFERENCES public.smart_phone(id)
                    );
                """
        };
    }

    @Override
    public String[] tables() {
        return new String[]{"public.smart_phone", "public.mobile_app"};
    }

    @Override
    public boolean runTableSetup() {
        return true;
    }

    @Override
    public String[] sequences() {
        return new String[]{"smart_phone_seq", "mobile_app_seq"};
    }
}

package com.rationaleemotions;

import com.rationaleemotions.domain.Directory;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.ParameterExpression;
import jakarta.persistence.criteria.Root;
import org.testng.annotations.Test;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class CriteriaBuilderDemoTest {

    @Test
    public void runDemo() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Directory.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                Long value = (Long) em.createNativeQuery("SELECT last_value from directory_id_seq;", Long.class).getSingleResult();
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                AtomicInteger counter = new AtomicInteger(value.intValue());
                IntStream.rangeClosed(1, 100)
                        .forEach(it -> {
                            Directory directory = new Directory().name("File_" + counter.getAndIncrement());
                            em.persist(directory);
                        });
                tx.commit();
            }

            //select * from Directory where id <= 10;
            System.err.println("Selectively printing some records record(s)");
            try (EntityManager em = emf.createEntityManager()) {
                //Build the query ONLY once and ensure that it has parameters so that it can be re-used multiple times
                // with different values.
                CriteriaQuery<Directory> query = findRecordsBetweenRange(em);

                System.err.println("Printing records whose id lies between 20 and 25");
                em.createQuery(query)
                        .setParameter("from", 20)
                        .setParameter("to", 25)
                        .getResultList()
                        .forEach(System.err::println);
                System.err.println("Printing records whose id lies between 40 and 43");
                em.createQuery(query)
                        .setParameter("from", 40)
                        .setParameter("to", 43)
                        .getResultList()
                        .forEach(System.err::println);

                System.err.println("Printing Specific name matching record whose id lies between 40 and 43");
                CriteriaQuery<Directory> q = findRecordsBetweenRangeMatchingName(em);
                em.createQuery(q)
                        .setParameter("from", 40)
                        .setParameter("to", 43)
                        .setParameter("name", "File_42")
                        .getResultList()
                        .forEach(System.err::println);
            }
        }
    }

    private static CriteriaQuery<Directory> findRecordsBetweenRangeMatchingName(EntityManager em) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Directory> query = builder.createQuery(Directory.class);
        Root<Directory> table = query.from(Directory.class);
        ParameterExpression<Long> from = builder.parameter(Long.class, "from");
        ParameterExpression<Long> to = builder.parameter(Long.class, "to");
        ParameterExpression<String> name = builder.parameter(String.class, "name");
        return query.select(table)
                .where(
                        builder.and(
                                builder.between(table.get("id"), from, to),
                                builder.equal(table.get("name"), name)
                        )
                );
    }

    private static CriteriaQuery<Directory> findRecordsBetweenRange(EntityManager em) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Directory> query = builder.createQuery(Directory.class);
        Root<Directory> table = query.from(Directory.class);
        ParameterExpression<Long> from = builder.parameter(Long.class, "from");
        ParameterExpression<Long> to = builder.parameter(Long.class, "to");
        return query.select(table).where(builder.between(table.get("id"), from, to));
    }
}

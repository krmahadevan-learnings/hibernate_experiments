package com.rationaleemotions;

import com.rationaleemotions.domain.*;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.util.Map;

/*

create table public.tech_lead(
    id serial NOT NULL PRIMARY KEY,
    name varchar(100) NOT NULL
);

create sequence tech_lead_seq increment by 50;

create table public.engineer(
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL,
    which_lead bigint,
    foreign key (which_lead) references public.tech_lead(id)
);

create sequence engineer_seq increment by 50;

 */
public class MapEntityDemoTest extends ManualSchemaSetupHelper {

    @Test
    public void demoMapCollection() {
        //This sample expects the required tables/sequences/relationships to be pre-created
        //Hence we are passing the strategy as NONE
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.NONE, SoftwareEngineer.class, TechnicalLead.class)) {
            Long id;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();

                TechnicalLead lead = new TechnicalLead().name("TL-1");

                SoftwareEngineer engineer1 = new SoftwareEngineer().lead(lead).name("SE-1");
                SoftwareEngineer engineer2 = new SoftwareEngineer().lead(lead).name("SE-2");
                em.persist(engineer1);
                em.persist(engineer2);
                lead.engineers(Map.of(engineer1.id(), engineer1, engineer2.id(), engineer2));
                em.persist(lead);
                tx.commit();
                id = lead.id();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(TechnicalLead.class, id));
            }
        }
    }

    @Override
    public String[] sequences() {
        return new String[]{"tech_lead_seq", "engineer_seq"};
    }

    @Override
    public String[] tables() {
        return new String[]{"public.tech_lead", "public.engineer"};
    }

    @Override
    public String[] queries() {
        return new String[]{
                "create table public.tech_lead(id serial NOT NULL PRIMARY KEY,name varchar(100) NOT NULL); ",
                """
                    create table public.engineer(
                            id serial NOT NULL PRIMARY KEY,
                            name varchar(255)NOT NULL,
                            which_lead bigint,
                            foreign key(which_lead)references public.tech_lead(id)
                    );
                """
        };
    }

    @Override
    public boolean runTableSetup() {
        return true;
    }
}

package com.rationaleemotions;

import com.rationaleemotions.domain.Food;
import com.rationaleemotions.domain.Taste;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

public class SecondaryTableDemoTest {

    @Test
    public void demo() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Food.class, Taste.class)) {
            long key;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();

                Food food = new Food()
                        .name("Falooda")
                        .bland(false)
                        .sweet(true)
                        .price(100.00F)
                        .hasOffer(false)
                        .mealType(Food.MealType.DESSERT)
                        .vegetarian(true);
                tx.begin();
                em.persist(food);
                key = food.id();
                tx.commit();
            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Food.class, key));
            }
        }
    }
}

package com.rationaleemotions;

import com.rationaleemotions.domain.*;
import com.rationaleemotions.persistence.HibernateUtils;
import com.rationaleemotions.persistence.Strategy;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class InheritanceDemoTest {

    @Test
    public void showInheritanceUsingMappedSuperClass() {
        try(EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Player.class, CricketBowler.class, FootballPlayer.class )) {
            Long bowlerId;
            Long footballId;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                CricketBowler bowler = new CricketBowler().name("Javagal Srinath")
                        .dateOfBirth(LocalDateTime.of(1973, 5, 24, 0, 0))
                        .oversBowled(100)
                        .wicketsTaken(500);
                FootballPlayer footballPlayer = new FootballPlayer().name("Maradona")
                        .dateOfBirth(LocalDateTime.of(1980, 2, 15, 0, 0))
                        .isDefence(false)
                        .isForward(true)
                        .goalsScored(100);
                em.persist(bowler);
                em.persist(footballPlayer);
                tx.commit();
                bowlerId = bowler.id();
                footballId = footballPlayer.id();
            }
            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(CricketBowler.class, bowlerId));
                System.err.println(em.find(FootballPlayer.class, footballId));
            }
        }
    }

    @Test
    public void showInheritanceWithSingleTable() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, ElectronicDevice.class, Calculator.class, Laptop.class)) {
            Long calculatorId;
            Long laptopId;
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Calculator calculator = new Calculator().scientific(false).name("Seiko").serialNumber("123");
                em.persist(calculator);
                Laptop laptop = new Laptop().ramSizeInGB(16).name("MacBookPro").serialNumber("987");
                em.persist(laptop);
                tx.commit();
                calculatorId = calculator.id();
                laptopId = laptop.id();
            }
            try (EntityManager em = emf.createEntityManager()) {
                System.err.println(em.find(Calculator.class, calculatorId));
                System.err.println(em.find(Laptop.class, laptopId));
            }
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void showInheritanceWithPerClass() {
        try (EntityManagerFactory emf = HibernateUtils.newInstance(Strategy.DROP_AND_CREATE, Animal.class, Mammal.class, Bird.class)) {
            try (EntityManager em = emf.createEntityManager()) {
                EntityTransaction tx = em.getTransaction();
                tx.begin();
                Bird bird = new Bird().name("Lux Dove").species(Species.BIRD).type(Bird.BirdType.DOVE).color("White");
                em.persist(bird);
                Mammal mammal = new Mammal().species(Species.MAMMAL).name("Dog").isDangerous(false).isDomesticable(true);
                em.persist(mammal);
                tx.commit();
            }

//            try (EntityManager em = emf.createEntityManager()) {
//                em.createQuery("select b from Bird b", Bird.class)
//                        .getResultList()
//                        .forEach(System.err::println);
//            }

            try (EntityManager em = emf.createEntityManager()) {
                System.err.println("Printing animals");
                em.createQuery("select a from Animal a", Animal.class)
                        .getResultStream()
                        .forEach(System.err::println);
                System.err.println("Printing birds");
                em.createQuery("select a from Bird a", Bird.class)
                        .getResultList()
                        .forEach(System.err::println);
                System.err.println("Printing mammals");
                em.createQuery("select a from Mammal a", Mammal.class)
                        .getResultList()
                        .forEach(System.err::println);
                System.err.println("Printing the latest inserted mammal");
                em.createQuery("select max(a.id) from Mammal a", Long.class)
                        .getResultList()
                        .forEach(System.err::println);

                System.err.println("Printing ONLY the names");
//                em.createQuery("select a.name from Mammal a", String.class)
//                        .getResultList()
//                        .forEach(System.err::println);
//

                em.createNativeQuery("select a.name from Mammal a", String.class)
                        .getResultList()
                        .forEach(System.err::println);
            }
        }

    }
}
